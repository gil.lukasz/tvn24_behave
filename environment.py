
from Browser.Browser import Browser, Browser_Action
from Pages.Rodo_Alert import Rodo
from Pages.Home_page import Tvn24_Home_page


def before_all(context):
    context.Browser_driver = Browser()
    context.Browser_action = Browser_Action()
    context.Tvn24_Home_page = Tvn24_Home_page()
    context.Rodo = Rodo()


def after_all(context):
    context.Browser_driver.close()
    print("End of Scenario")
