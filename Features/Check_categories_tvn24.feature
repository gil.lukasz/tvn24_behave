Feature: Each category of tvn24 should be displayed properly

    Scenario Outline: Click on category on tvn24 should display category page properly
      Given   User is Unlogged on Tvn24 Home page
      When    User "click" <category_button> button
      Then    Url should be <category_url>
      And     Page should be "visible"

      Examples:
        | category_button      | category_url                |
        | NAJNOWSZE     | https://tvn24.pl/najnowsze|
        | FAKTY         | https://fakty.tvn24.pl|
        | TVN24 GO      | https://tvn24.pl/go     |
        | POLSKA        | https://tvn24.pl/polska   |
        | ŚWIAT         | https://tvn24.pl/swiat   |
        | PREMIUM       | https://tvn24.pl/premium   |
        | MAGAZYN TVN24 | https://tvn24.pl/tvn24-magazyn/polska-kryminalna   |
        | SPORT         | https://eurosport.tvn24.pl |
        | BIZNES        | https://tvn24.pl/biznes    |
        | METEO         | https://tvn24.pl/tvnmeteo  |
        | KONKRET24     | https://konkret24.tvn24.pl |
        | KONTAKT24     | https://kontakt24.tvn24.pl |
        | KULTURA I STYL  | https://tvn24.pl/kultura-i-styl   |
        | SZKŁO KONTAKTOWE| https://szklokontaktowe.tvn24.pl   |
        | CIEKAWOSTKI   | https://tvn24.pl/ciekawostki    |
        | PROGRAMY      | https://tvn24.pl/programy  |
        | RAPORTY       | https://tvn24.pl/raporty   |
        | TVN24 NEWS IN ENGLISH | https://tvn24.pl/tvn24-news-in-english |
        | WOŚP 2021     | https://tvn24.pl/wosp-2021  |

