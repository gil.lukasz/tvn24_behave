
from behave import *

@given('User is Unlogged on Tvn24 Home page')
def test_step_chrome_browser_and_tvn24_page_start(context):
    context.Browser_action.Page_open_url("https://tvn24.pl/")
    context.Rodo.onetrust_rodo_accept()
    context.Tvn24_Home_page.tvn24_Page_Load_Check()
    context.Tvn24_Home_page.tvn24_header_position_check()


@when('User "click" {category_button} button')
def step_find_and_click_category_button(context, category_button):
    context.Tvn24_Home_page.find_and_click_category_button(category_button)
    context.Rodo.rodo_cookies_presence()

@then('Url should be {category_url}')
def step_url_assertion(context, category_url):
    context.Browser_action.url_contains_check(category_url)

@then('Page should be "visible"')
def step_page_vissible_check(context):
    pass
