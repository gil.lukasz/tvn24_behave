from allure_commons.types import AttachmentType
from behave import given, when, then


@given('User open Chrome Browser_driver')
def step_chrome_browser_start(context):
    context.Browser_action.Page_open_url("https://tvn24.pl/")

@when('User open tvn24 home page')
def step_start_tvn24_page(context):
    context.Rodo.rodo_cookies_presence()
    context.Tvn24_Home_page.tvn24_Page_Load_Check()
    context.Tvn24_Home_page.tvn24_header_position_check()

@then('Tvn24 service is visible for user')
def step_tvn24_page_visible_check(context):
    context.Tvn24_Home_page.tvn24_Page_Load_Check()