import time
import allure
from allure_commons.types import AttachmentType
import datetime
from datetime import datetime

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains as AC

from Browser.Browser import Browser, Browser_Action


class Tvn24_Home_page(Browser):

    def __init__(self):
        Browser_Action.__init__(self)
        self.date_hour = datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.CSS_header = "li.header__list-item.header__list-item--menu"
        self.Css_more_button = "header button.header-menu__more-button"

        self.CSS_visible_buttons = "header ul.header-menu__row-list>li"
        self.visible_button_class = "header-menu__item"
        self.CSS_hidden_buttons = "header ul.header-menu__more-list-elements>li"
        self.hidden_class = "header-menu__item.header-menu__item--hidden"
        self.submenu_button_class = "header-menu__item header-menu__item--with-submenu"

        self.header = (By.CSS_SELECTOR, 'header.header')
        self.account_button = (By.CSS_SELECTOR, "button.account-standard__toggle-button")
        self.Css_rodo = (By.CSS_SELECTOR, "button#onetrust-accept-btn-handler")


    @allure.step("Weryfikacja załadowania strony tvn24.pl")
    def tvn24_Page_Load_Check(self):
        print("Page load chceck")
        driver = self.driver
        for approach in range(5):
            try:
                Wait(driver, 2, 1).until(EC.presence_of_element_located(self.account_button))
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Tvn24 Main page",
                              attachment_type=AttachmentType.PNG)
                print('Main page Tvn24 - load success')
                return
            except:
                continue
        else:
            print('Main page Tvn24 - load Fail')

    @allure.step("Szukanie i klikniecie w przycisk szukanej kategorii")
    def find_and_click_category_button(self, category_button):
        More_Button = self.driver.find_element(By.CSS_SELECTOR, self.Css_more_button)
        Header_Menu = self.driver.find_element(By.CSS_SELECTOR, self.CSS_header)

        self.driver.execute_script("arguments[0].scrollIntoView();", Header_Menu)
        time.sleep(1)

        print(f"Searching '{category_button}' button in vissible buttons")
        buttons = self.driver.find_elements(By.CSS_SELECTOR, self.CSS_visible_buttons)
        for button in buttons:
            if button.get_attribute("class") != self.hidden_class:
                button_link = button.find_element(By.TAG_NAME, "a")
                if button_link.get_attribute("innerText") == category_button:
                    print(f"User click on '{category_button}' button in vissible buttons")
                    time.sleep(1)
                    AC(self.driver).move_to_element(button_link).perform()
                    time.sleep(1)
                    allure.attach(self.driver.get_screenshot_as_png(),
                                  name=f"Button category - {category_button}",
                                  attachment_type=AttachmentType.PNG)
                    self.driver.get(button_link.get_attribute("href"))
                    Wait(self.driver, 3, 0.5).until(EC.url_changes)
                    return
                else:
                    continue
            else:
                continue

        print(f"Searching '{category_button}' button in hidden buttons")
        hide_buttons = self.driver.find_elements(By.CSS_SELECTOR, self.CSS_hidden_buttons)
        for hide_button in hide_buttons:
            button_hidden_link = hide_button.find_element(By.TAG_NAME, "a")
            if button_hidden_link.get_attribute("innerText") == category_button:
                print(f"User click on '{category_button}' button in hidden buttons")
                AC(self.driver).move_to_element(More_Button).perform()
                time.sleep(1)
                AC(self.driver).move_to_element(button_hidden_link).perform()
                time.sleep(1)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Button category - {category_button}",
                              attachment_type=AttachmentType.PNG)
                self.driver.get(button_hidden_link.get_attribute("href"))
                Wait(self.driver, 3, 0.5).until(EC.url_changes)
                return
            else:
                continue

    @allure.step("Weryfikacja pełnego załadowania górnej reklamy")
    def tvn24_header_position_check(self):
        driver = self.driver
        header_Start_location = driver.find_element(*self.header).location
        print(f"Header Start location : {header_Start_location}")
        Check_Counter = 1
        Start_loc_counter = 0
        Expand_loc_counter = 0
        for attempt in range(15):
            header_expand_location = driver.find_element(*self.header).location
            print(f"Attempt : {Check_Counter} - Header Second location : {header_expand_location}")
            if header_expand_location == header_Start_location:
                Start_loc_counter += 1
                Check_Counter += 1
                if Start_loc_counter <= 5:
                    print(f"Header don't change position in {Start_loc_counter} check")
                    time.sleep(0.5)
                    continue
                else:
                    print(f"Header don't change position after 5 check attempts")
                    return
            elif header_expand_location != header_Start_location:
                Expand_loc_counter += 1
                Check_Counter += 1
                if Expand_loc_counter <= 5:
                    print(f"Header don't change position in {Expand_loc_counter} check")
                    time.sleep(0.5)
                    continue
                else:

                    print(f"Header change position after 5 check attempts")
                    return