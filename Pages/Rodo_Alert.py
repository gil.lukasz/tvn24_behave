import datetime
import time
from datetime import datetime

import allure
from allure_commons.types import AttachmentType
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait

from Browser.Browser import Browser


class Rodo(Browser):

    def __init__(self):

        self.date_hour = datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.Css_rodo = (By.CSS_SELECTOR, "button#onetrust-accept-btn-handler")

        self.urgent_article = (By.ID, 'urgentStandard')

        self.date_hour = datetime.now().strftime("%d-%m-%Y %H_%M_%S.%f")
        self.CSS_header = "li.header__list-item--menu"
        self.CSS_header_categories_visible = "header .header-menu__row-list>li"

        self.logo = (By.CSS_SELECTOR, "a.brand-tvn")
        self.eurosport_logo = (By.CSS_SELECTOR, "img.nav__back-button-logo")
        self.meteo_logo = (By.CSS_SELECTOR, "#jLogo")
        self.konkret24_logo = (By.CSS_SELECTOR, "figure.header-left-corner__logo")


    @allure.step("Sprawdzenie obecnosci ciasteczka Rodo")
    def rodo_cookies_presence(self):
        driver = self.driver
        for approach in range(5):
            cookies = driver.get_cookies()
            for cookie in cookies:
                if cookie['name'] == 'ObptanonAlertBoxClosed':
                    break
                else:
                    continue
        else:
            self.onetrust_rodo_accept()

    @allure.step("Akceptacja i czekanie na Rodo")
    def onetrust_rodo_accept(self):
        print("Wait for rodo alert")
        driver = self.driver
        attempt = 1
        for approach in range(5):
            try:
                accept_button = Wait(driver, 2, 0.5).until(EC.presence_of_element_located(self.Css_rodo))
                driver.execute_script("return arguments[0].scrollIntoView(true);", accept_button)
                allure.attach(self.driver.get_screenshot_as_png(),
                              name=f"Rodo_alert - on {self.driver.current_url}",
                              attachment_type=AttachmentType.PNG)
                accept_button.click()
                print(f'Rodo Approval - after {attempt} attempt')
                break
            except TimeoutException:
                # print(f'Rodo alert not found - after {attempt} attempt')
                attempt += 1
                continue
        else:
            print(f'Rodo alert - was not displayed after {attempt} attempt')
            allure.attach(self.driver.get_screenshot_as_png(),
                          name=f"No presence of Rodo_alert - on {self.driver.current_url}",
                          attachment_type=AttachmentType.PNG)
