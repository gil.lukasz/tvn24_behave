from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import os
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains as AC


class Browser(object):
    options = Options()
    options.page_load_strategy = 'normal'
    chrome_options = webdriver.ChromeOptions()

    if os.name == 'nt':
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
        print("Chrome")
        driver.maximize_window()
        # driver.set_page_load_timeout(30)

    else:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        driver = webdriver.Chrome(options=chrome_options)

    def close(context):
        context.driver.close()

    def quit(context):
        context.driver.quit()


class Browser_Action(Browser):

    def Page_open_url(self, url):
        self.driver.get(url)
        print(f"Open_page : {url}")
        Wait(self.driver, 3, 0.5).until(EC.url_changes)

    def get_page_title(self):
        return self.driver.title

    def get_page_url(self):
        return self.driver.current_url

    def url_contains_check(self, url):
        current_url = self.get_page_url()
        if url in current_url:
            print(f"Current url :{current_url} contain exepexted url : {url} ")
            return
        else:
            print(f"Current url :{current_url} is different than expected url : {url} ")

    def hoover(self, locator):
        AC(self.driver).move_to_element(locator).perform()

    def click(self, locator):
        AC(self.driver).click(locator).perform()



    # options = Options()
    # options.page_load_strategy = 'normal'
    # chrome_options = webdriver.ChromeOptions()
    #
    # if os.name == 'nt':
    #     driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    #     print("Rodo Chrome")
    #     driver.maximize_window()
    #     # driver.set_page_load_timeout(15)
    #
    # else:
    #     chrome_options.add_argument('--no-sandbox')
    #     chrome_options.add_argument('--headless')
    #     chrome_options.add_argument('--disable-gpu')
    #     driver = webdriver.Chrome(options=chrome_options)
    #     # request.cls.driver = context.driver
